package com.example.tlogexampleparamprint;

import com.yomahub.tlog.core.annotation.TLogAspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;


@SpringBootApplication
@EnableAsync
@RestController
public class RunnerApplication {

    private Logger log = LoggerFactory.getLogger(this.getClass());

    public static void main(String[] args) {
        SpringApplication.run(RunnerApplication.class, args);
    }

    @RequestMapping("/abc/upload")
    public String sayHello(@RequestParam("file") MultipartFile file){
        log.info("invoke method sayHello");
        String fileName = file.getOriginalFilename();
        log.info("fileName:{}",fileName);
        return "hello";
    }
}
